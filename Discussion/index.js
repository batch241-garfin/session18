
// Parameter acts as a named variable/container that exists
// only inside of a function
// It is used to store informtaion that is provided to
// a function when it is called/invoked

// Syntax:
// 	function functionName(parameter)
// {
// 	Statements/block
// }
// invokedFunction

function printName(name)
{
	console.log("My name is "+ name);
}
printName("Romhel");
printName("Romhelson");

// // Variables can also be passed as an argument

let userName ="sds";

printName(userName);

// The output is Hello, User!

function greeting()
{
	console.log("Hello, User!");
}
greeting("Garfin");

// // Using Multiple Parameters

function createFullName(firstName, middleName, lastName)
{
	console.log(firstName + " "+middleName+" "+lastName);
}

// In javascript, providing more/less arguments than the
// expected parameters will not return an errror.

createFullName("Romhel","Aspe","Garfin");
createFullName("Eric","Andales"); // Undefined

function checkDivisibilityBy8(num)
{
	let remainder = num % 8;
	console.log("The remainder of "+ num + " divided by 8 is "+remainder);

	let isDivisibleBy8 = (remainder === 0);
	console.log("Is "+ num +" divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// Mini Activity //

let myFave = prompt("Enter your favorite superhero");

function myFaveSuperhero(superHero)
{
	console.log("My favorite superhero is "+superHero);
}
myFaveSuperhero(myFave); // Batman

// Return Statement //

	// It allows us to output a value from a function
	// to be passed to the line/block of code that
	// invoked/called the function.

function returnFullName(firstName, middleName, lastName)
{
	return firstName + " " + middleName + " " + lastName;
	console.log("Can we print this message?");
}

let completeName = returnFullName("R","A","G");

console.log(completeName);


function returnAddress(city, country)
{	
	let fullAddress = city + ", " + country;
	return fullAddress;
}

let myAddress = returnAddress("Daet","Philippines");
console.log(myAddress);


// function printPlayerInformation(userName, level, role)
// {
// 	console.log("Username: " + userName);
// 	console.log("Level: " + level);
// 	console.log("Role: " + role);
// }

// let user1 = printPlayerInformation("lehmour","998","Demon");
// console.log(user1); // Undefined


// Mini - Activity //

function printPlayerInformation(userName, level, race)
{
	
	return "Username:  "+ userName + "\nLevel: " + level + "\nRace: " + race;
}

let user1 = printPlayerInformation("Lehmour","998","Demon");
console.log(user1);