
// Addition //

function sumOfTwoNumbers(numOne, numTwo)
{
	let sum = numOne + numTwo;
	console.log("The sum of "+numOne+" + "+numTwo+" is: "+ sum);
}
sumOfTwoNumbers(27,64);

// Subtraction //

function differenceOfTwoNumbers(numOne, numTwo)
{
	let difference = numOne - numTwo;
	console.log("The difference of "+numOne+" - "+numTwo+" is: "+ difference);
}
differenceOfTwoNumbers(73,48);

// Multiplication //

function productOfTwoNumbers(numOne, numTwo)
{
	let product1 = numOne * numTwo;
	let displayProduct = "The product of "+numOne+" * "+numTwo+" is: "+ product1;
	return displayProduct;
}
let product2 = productOfTwoNumbers(76,18);
console.log(product2);

// Division //

function quotientOfTwoNumbers(numOne, numTwo)
{
	let quotient = numOne / numTwo;
	let displayQuotient = "The quotient of "+numOne+" / "+numTwo+" is: "+ quotient;
	return displayQuotient;
}
let quotient2 = quotientOfTwoNumbers(62,5);
console.log(quotient2);

// Area of a Circle //

function areaOfACircle(radius)
{
	let area = 3.14 * (radius * radius);
	let displayArea = "The area of a circle with a radius of "+radius+" is "+area;
	return displayArea;
}
let circleArea = areaOfACircle(342);
console.log(circleArea);

// Average of Four Numbers //

function avgOfFourNum(numOne, numTwo, numThree, numFour)
{
	let average1 = ((numOne + numTwo + numThree + numFour) / 4);
	let displayAverage = "The average of the four numbers  is: "+ average1;
	return displayAverage;
}
let averageVar = avgOfFourNum(96,88,94,99);
console.log(averageVar);

// Percentage of Passing //

function percentPassed(score, totalScore)
{
	let percentage = ((score / totalScore) * 100);
	let isPassed = "Passed: "+ (percentage > 75);
	return isPassed;
}

let scorePass = percentPassed(45, 100);
console.log(scorePass);